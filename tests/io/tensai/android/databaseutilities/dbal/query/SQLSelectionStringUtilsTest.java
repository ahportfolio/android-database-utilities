/*
 *     Copyright (C) 2016  Aston Hamilton
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.tensai.android.databaseutilities.dbal.query;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;

/**
 * Created by Aston Hamilton
 */
public class SQLSelectionStringUtilsTest {

    @Test
    public void testCriteria() {
        final String CRITERIA_RESULT = "" +
                "(col = ?" +
                " AND " +
                "(" +
                "(col != ? AND col = ?)" +
                " OR " +
                "(col IN (?, ?) AND (col2 = ? OR col <> ?))" +
                " OR " +
                "(col2 < ? AND col > ?)" +
                " OR " +
                "(col <= ? AND col2 >= ?)" +
                " OR " +
                "col BETWEEN ? AND ?" +
                " OR " +
                "col LIKE ?" +
                " OR " +
                "col2 NOT BETWEEN ? AND ?" +
                " OR " +
                "col NOT LIKE ?" +
                ")" +
                " AND " +
                "col NOT IN (?, ?))";
        final String[] CRITERIA_ARGS = new String[]{
                "val1",
                "neqVal1", "eqVal1",
                "inVal1", "inVal2", "eqVal2", "neqAlt",
                "4", "5",
                "9", "6",
                "2", "16",
                "likeVal",
                "8", "79",
                "notLikeVal",
                "inVal1", "inVal2"
        };

        final SQLSelectionStringUtils.StringFragmentBuilder.ColumnExpressionBuilder builder = SQLSelectionStringUtils.StringFragmentBuilder.forColumn("col");
        final SQLSelectionStringUtils.StringFragmentBuilder.ColumnExpressionBuilder builder2 = SQLSelectionStringUtils.StringFragmentBuilder.forColumn("col2");

        ArrayList<String> args = new ArrayList<>();

        Assert.assertEquals(
                CRITERIA_RESULT,
                SQLSelectionStringUtils.buildSelectionFromFragments(
                        SQLSelectionStringUtils.StringFragmentBuilder.and(
                                builder.eq("val1", args),
                                SQLSelectionStringUtils.StringFragmentBuilder.or(
                                        SQLSelectionStringUtils.StringFragmentBuilder.and(
                                                builder.neq("neqVal1", args),
                                                builder.eq("eqVal1", args)
                                        ),
                                        SQLSelectionStringUtils.StringFragmentBuilder.and(
                                                builder.in(new String[]{"inVal1", "inVal2"}, args),
                                                SQLSelectionStringUtils.StringFragmentBuilder.or(
                                                        builder2.eq("eqVal2", args),
                                                        builder.neqAlt("neqAlt", args)
                                                )
                                        ),
                                        SQLSelectionStringUtils.StringFragmentBuilder.and(
                                                builder2.lt(String.valueOf(4), args),
                                                builder.gt(String.valueOf(5), args)
                                        ),
                                        SQLSelectionStringUtils.StringFragmentBuilder.and(
                                                builder.lte(String.valueOf(9), args),
                                                builder2.gte(String.valueOf(6), args)
                                        ),
                                        builder.between(String.valueOf(2), String.valueOf(16), args),
                                        builder.like("likeVal", args),
                                        builder2.notBetween(String.valueOf(8), String.valueOf(79), args),
                                        builder.notLike("notLikeVal", args)
                                ),
                                builder.notIn(new String[]{"inVal1", "inVal2"}, args)
                        )
                )
        );
        Assert.assertArrayEquals(CRITERIA_ARGS, args.toArray());

        args = new ArrayList<>();
        Assert.assertEquals(
                CRITERIA_RESULT,
                SQLSelectionStringUtils.buildSelectionFromFragments(
                        SQLSelectionStringUtils.StringFragmentBuilder.and(
                                SQLSelectionStringUtils.StringFragmentBuilder.eq("col", "val1", args),
                                SQLSelectionStringUtils.StringFragmentBuilder.or(
                                        SQLSelectionStringUtils.StringFragmentBuilder.and(
                                                SQLSelectionStringUtils.StringFragmentBuilder.neq("col", "neqVal1", args),
                                                SQLSelectionStringUtils.StringFragmentBuilder.eq("col", "eqVal1", args)
                                        ),
                                        SQLSelectionStringUtils.StringFragmentBuilder.and(
                                                SQLSelectionStringUtils.StringFragmentBuilder.in("col", new String[]{"inVal1", "inVal2"}, args),
                                                SQLSelectionStringUtils.StringFragmentBuilder.or(
                                                        SQLSelectionStringUtils.StringFragmentBuilder.eq("col2", "eqVal2", args),
                                                        SQLSelectionStringUtils.StringFragmentBuilder.neqAlt("col", "neqAlt", args)
                                                )
                                        ),
                                        SQLSelectionStringUtils.StringFragmentBuilder.and(
                                                SQLSelectionStringUtils.StringFragmentBuilder.lt("col2", String.valueOf(4), args),
                                                SQLSelectionStringUtils.StringFragmentBuilder.gt("col", String.valueOf(5), args)
                                        ),
                                        SQLSelectionStringUtils.StringFragmentBuilder.and(
                                                SQLSelectionStringUtils.StringFragmentBuilder.lte("col", String.valueOf(9), args),
                                                SQLSelectionStringUtils.StringFragmentBuilder.gte("col2", String.valueOf(6), args)
                                        ),
                                        SQLSelectionStringUtils.StringFragmentBuilder.between("col", String.valueOf(2), String.valueOf(16), args),
                                        SQLSelectionStringUtils.StringFragmentBuilder.like("col", "likeVal", args),
                                        SQLSelectionStringUtils.StringFragmentBuilder.notBetween("col2", String.valueOf(8), String.valueOf(79), args),
                                        SQLSelectionStringUtils.StringFragmentBuilder.notLike("col", "notLikeVal", args)
                                ),
                                SQLSelectionStringUtils.StringFragmentBuilder.notIn("col", new String[]{"inVal1", "inVal2"}, args)
                        )
                )
        );
        Assert.assertArrayEquals(CRITERIA_ARGS, args.toArray());
    }
}