/*
 *     Copyright (C) 2016  Aston Hamilton
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.tensai.android.databaseutilities.dbal.query;

import java.util.Collections;
import java.util.List;

/**
 * Created by Aston Hamilton
 */
public abstract class SQLSelectionStringUtils {
    public static class StringFragmentBuilder {
        public static class ColumnExpressionBuilder {
            private final String mColumnName;

            private ColumnExpressionBuilder(final String columnName) {
                mColumnName = columnName;
            }

            public String[] eq(final String value, final List<String> argDest) {
                return StringFragmentBuilder.eq(mColumnName, value, argDest);
            }

            public String[] neqAlt(final String value, final List<String> argDest) {
                return StringFragmentBuilder.neqAlt(mColumnName, value, argDest);
            }

            public String[] neq(final String value, final List<String> argDest) {
                return StringFragmentBuilder.neq(mColumnName, value, argDest);
            }

            public String[] lt(final String value, final List<String> argDest) {
                return StringFragmentBuilder.lt(mColumnName, value, argDest);
            }

            public String[] gt(final String value, final List<String> argDest) {
                return StringFragmentBuilder.gt(mColumnName, value, argDest);
            }

            public String[] lte(final String value, final List<String> argDest) {
                return StringFragmentBuilder.lte(mColumnName, value, argDest);
            }

            public String[] gte(final String value, final List<String> argDest) {
                return StringFragmentBuilder.gte(mColumnName, value, argDest);
            }

            public String[] like(final String value, final List<String> argDest) {
                return StringFragmentBuilder.like(mColumnName, value, argDest);
            }

            public String[] notLike(final String value, final List<String> argDest) {
                return StringFragmentBuilder.notLike(mColumnName, value, argDest);
            }

            public String[] between(final String value1, final String value2, final List<String> argDest) {
                return StringFragmentBuilder.between(mColumnName, value1, value2, argDest);
            }

            public String[] notBetween(final String value1, final String value2, final List<String> argDest) {
                return StringFragmentBuilder.notBetween(mColumnName, value1, value2, argDest);
            }

            public String[] in(final String[] options, final List<String> argDest) {
                return StringFragmentBuilder.in(mColumnName, options, argDest);
            }

            public String[] notIn(final String[] options, final List<String> argDest) {
                return StringFragmentBuilder.notIn(mColumnName, options, argDest);
            }
        }

        public static ColumnExpressionBuilder forColumn(final String columnName) {
            return new ColumnExpressionBuilder(columnName);
        }

        public static String[] eq(final String columnName, final String value, final List<String> argDest) {
            if (argDest == null) {
                return new String[]{
                        columnName, " = ", value
                };
            }

            argDest.add(value);
            return new String[]{
                    columnName, " = ?"
            };
        }

        public static String[] neqAlt(final String columnName, final String value, final List<String> argDest) {
            if (argDest == null) {
                return new String[]{
                        columnName, " <> ", value
                };
            }

            argDest.add(value);
            return new String[]{
                    columnName, " <> ?"
            };
        }

        public static String[] neq(final String columnName, final String value, final List<String> argDest) {
            if (argDest == null) {
                return new String[]{
                        columnName, " != ", value
                };
            }

            argDest.add(value);
            return new String[]{
                    columnName, " != ?"
            };
        }


        public static String[] lt(final String columnName, final String value, final List<String> argDest) {
            if (argDest == null) {
                return new String[]{
                        columnName, " < ", value
                };
            }

            argDest.add(value);
            return new String[]{
                    columnName, " < ?"
            };
        }

        public static String[] gt(final String columnName, final String value, final List<String> argDest) {
            if (argDest == null) {
                return new String[]{
                        columnName, " > ", value
                };
            }

            argDest.add(value);
            return new String[]{
                    columnName, " > ?"
            };
        }

        public static String[] lte(final String columnName, final String value, final List<String> argDest) {
            if (argDest == null) {
                return new String[]{
                        columnName, " <= ", value
                };
            }

            argDest.add(value);
            return new String[]{
                    columnName, " <= ?"
            };
        }

        public static String[] gte(final String columnName, final String value, final List<String> argDest) {
            if (argDest == null) {
                return new String[]{
                        columnName, " >= ", value
                };
            }

            argDest.add(value);
            return new String[]{
                    columnName, " >= ?"
            };
        }

        public static String[] like(final String columnName, final String value, final List<String> argDest) {
            if (argDest == null) {
                return new String[]{
                        columnName, " LIKE ", value
                };
            }

            argDest.add(value);
            return new String[]{
                    columnName, " LIKE ?"
            };
        }

        public static String[] notLike(final String columnName, final String value, final List<String> argDest) {
            if (argDest == null) {
                return new String[]{
                        columnName, " NOT LIKE ", value
                };
            }

            argDest.add(value);
            return new String[]{
                    columnName, " NOT LIKE ?"
            };
        }

        public static String[] between(final String columnName, final String value1, final String value2, final List<String> argDest) {
            if (argDest == null) {
                return new String[]{
                        columnName, " BETWEEN ", value1, " AND ", value2
                };
            }

            argDest.add(value1);
            argDest.add(value2);
            return new String[]{
                    "", columnName, " BETWEEN ? AND ?"
            };
        }

        public static String[] notBetween(final String columnName, final String value1, final String value2, final List<String> argDest) {
            if (argDest == null) {
                return new String[]{
                        columnName, " NOT BETWEEN ", value1, " AND ", value2
                };
            }

            argDest.add(value1);
            argDest.add(value2);
            return new String[]{
                    columnName, " NOT BETWEEN ? AND ?"
            };
        }

        public static String[] in(final String columnName, final String[] options, final List<String> argDest) {
            final String[] fragments;
            if (options.length == 0) {
                return new String[]{
                        columnName, " IN ()"
                };
            }

            if (argDest == null) {
                fragments = new String[3 + ((options.length * 2) - 1)];
                fragments[2] = options[0];
                //noinspection ForLoopReplaceableByForEach
                for (int i = 1, optionsLength = options.length; i < optionsLength; i++) {
                    fragments[2 + i] = ", ";
                    fragments[3 + i] = options[i];
                }
            } else {
                fragments = new String[3 + options.length];
                fragments[2] = "?";
                for (int i = 1, optionsLength = options.length; i < optionsLength; i++) {
                    fragments[2 + i] = ", ?";
                }

                Collections.addAll(argDest, options);
            }

            fragments[0] = columnName;
            fragments[1] = " IN (";
            fragments[fragments.length - 1] = ")";

            return fragments;
        }

        public static String[] notIn(final String columnName, final String[] options, final List<String> argDest) {
            final String[] fragments;
            if (options.length == 0) {
                return new String[]{
                        columnName, " NOT IN ()"
                };
            }

            if (argDest == null) {
                fragments = new String[3 + ((options.length * 2) - 1)];
                fragments[2] = options[0];
                //noinspection ForLoopReplaceableByForEach
                for (int i = 1, optionsLength = options.length; i < optionsLength; i++) {
                    fragments[2 + i] = ", ";
                    fragments[3 + i] = options[i];
                }
            } else {
                fragments = new String[3 + options.length];
                fragments[2] = "?";
                for (int i = 1, optionsLength = options.length; i < optionsLength; i++) {
                    fragments[2 + i] = ", ?";
                }

                Collections.addAll(argDest, options);
            }

            fragments[0] = columnName;
            fragments[1] = " NOT IN (";
            fragments[fragments.length - 1] = ")";

            return fragments;
        }

        public static String[] and(final String[]... inclusiveFragments) {
            final String[] fragments;
            int finalFragmentLength = 2 + inclusiveFragments.length - 1;
            int nextInsertionIndex = 1;

            //noinspection ForLoopReplaceableByForEach
            for (int i = 0, alternativeFragmentsLength = inclusiveFragments.length; i < alternativeFragmentsLength; i++) {
                final String[] alternativeFragment = inclusiveFragments[i];

                finalFragmentLength += alternativeFragment.length;
            }

            fragments = new String[finalFragmentLength];
            //noinspection ForLoopReplaceableByForEach
            for (int i = 0, alternativeFragmentsLength = inclusiveFragments.length; i < alternativeFragmentsLength; i++) {
                final String[] alternativeFragment = inclusiveFragments[i];

                System.arraycopy(alternativeFragment, 0, fragments, nextInsertionIndex, alternativeFragment.length);
                fragments[nextInsertionIndex + alternativeFragment.length] = " AND ";
                nextInsertionIndex += alternativeFragment.length + 1;
            }

            fragments[0] = "(";
            fragments[fragments.length - 1] = ")";
            return fragments;
        }

        public static String[] or(final String[]... alternativeFragments) {
            final String[] fragments;
            int finalFragmentLength = 2 + alternativeFragments.length - 1;
            int nextInsertionIndex = 1;

            //noinspection ForLoopReplaceableByForEach
            for (int i = 0, alternativeFragmentsLength = alternativeFragments.length; i < alternativeFragmentsLength; i++) {
                final String[] alternativeFragment = alternativeFragments[i];

                finalFragmentLength += alternativeFragment.length;
            }

            fragments = new String[finalFragmentLength];
            //noinspection ForLoopReplaceableByForEach
            for (int i = 0, alternativeFragmentsLength = alternativeFragments.length; i < alternativeFragmentsLength; i++) {
                final String[] alternativeFragment = alternativeFragments[i];

                System.arraycopy(alternativeFragment, 0, fragments, nextInsertionIndex, alternativeFragment.length);
                fragments[nextInsertionIndex + alternativeFragment.length] = " OR ";
                nextInsertionIndex += alternativeFragment.length + 1;
            }

            fragments[0] = "(";
            fragments[fragments.length - 1] = ")";
            return fragments;
        }

        public static String[] not(final String[] negatedFragment) {
            final String[] fragments = new String[2 + negatedFragment.length];
            fragments[0] = "NOT (";

            System.arraycopy(negatedFragment, 0, fragments, 1, negatedFragment.length);

            fragments[fragments.length - 1] = ")";

            return fragments;
        }
    }

    public static String buildSelectionFromFragments(final String[] fragments) {
        final StringBuilder selectionBuilder;
        int totalSelectionLength = 0;

        //noinspection ForLoopReplaceableByForEach
        for (int i = 0, fragmentLength = fragments.length; i < fragmentLength; i++) {
            totalSelectionLength += fragments[i].length();
        }

        selectionBuilder = new StringBuilder(totalSelectionLength);
        //noinspection ForLoopReplaceableByForEach
        for (int i = 0, fragmentsLength = fragments.length; i < fragmentsLength; i++) {
            selectionBuilder.append(fragments[i]);
        }

        return selectionBuilder.toString();
    }
}
