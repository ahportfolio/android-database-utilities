/*
 *     Copyright (C) 2016  Aston Hamilton
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.tensai.android.databaseutilities.dbal.repository;

import android.content.Context;
import io.tensai.android.databaseutilities.interfaces.repository.IEntityRepository;
import io.tensai.android.databaseutilities.interfaces.repository.IEntityRepositoryFactory;

/**
 * Created by aston
 */
public final class EntityManager {
    private static IEntityRepositoryFactory INSTANCE_FACTORY;

    public static <T> IEntityRepository<T> getEntityRepository(final Context context, final Class<T> entityClazz) {
        if (INSTANCE_FACTORY == null) {
            INSTANCE_FACTORY = (IEntityRepositoryFactory) new DefaultEntityRepositoryFactory();
        }

        if (!INSTANCE_FACTORY.isEntitySupported(entityClazz)) {
            return null;
        }

        return INSTANCE_FACTORY.buildEntityRepository(context, entityClazz);
    }
}
