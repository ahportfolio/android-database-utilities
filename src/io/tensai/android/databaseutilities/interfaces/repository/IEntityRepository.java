/*
 *     Copyright (C) 2016  Aston Hamilton
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.tensai.android.databaseutilities.interfaces.repository;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.List;

/**
 * Created by Aston Hamilton
 */
public interface IEntityRepository<T> {
    Cursor findByPkWithCursorResult(final long pk);

    Cursor findAllWithCursorResult();

    Cursor findAllWithCursorResult(final String orderBy);

    Cursor findAllWithCursorResult(final String orderBy, final int offset, final int limit);

    Cursor findWithCursorResult(final String selection, String[] selectionArgs);

    Cursor findWithCursorResult(final String selection, String[] selectionArgs, final String orderBy);

    Cursor findWithCursorResult(final String selection, String[] selectionArgs, final String orderBy, final int offset, final int limit);

    long insert(final ContentValues values);

    long insertOrThrow(final ContentValues values);

    long insertWithOnConflict(final ContentValues values, final int conflictAlgorithm);

    int updateAll(final ContentValues values);

    int updateByPk(final long pk, final ContentValues values);

    int update(final ContentValues values, final String whereClause, String[] whereArgs);

    int updateWithOnConflict(final ContentValues values, final String whereClause, String[] whereArgs, final int conflictAlgorithm);

    int deleteAll();

    int deleteByPk(final long pk);

    int delete(final String whereClause, String[] whereArgs);

    long countAll();

    long count(final String selection, String[] selectionArgs);

    T findByPk(final long pk);

    List<T> findAll();

    List<T> findAll(final String orderBy);

    List<T> findAll(final String orderBy, final int offset, final int limit);

    List<T> find(final String selection, String[] selectionArgs);

    List<T> find(final String selection, String[] selectionArgs, final String orderBy);

    List<T> find(final String selection, String[] selectionArgs, final String orderBy, final int offset, final int limit);

    SQLiteOpenHelper getOpenHelper();

    void beginTransaction();

    void setTransactionSuccessful();

    void endTransaction();

    T buildEntityFromRepositoryCursor(final Cursor cursor);

    void close();
}
