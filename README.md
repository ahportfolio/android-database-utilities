# android-database-utilities

Android DBALs are arguably cumbersome to use and add unnecessary overhead to what should be a fairly simple workflow.
I have found that my style of persistent client data management usually renders much of these "bloated" DBALs underused
and bothersome when the process need not be. This implementation of a DBAL for Android assumes a flat data model with
little dependence on foreign key relationships, and whole-entity oriented data access. This implementation uses
Java Annotation processing to create an easy to use DBAL with ZERO runtime overhead.

## How to install

This implementation is organized into two separate Java Libraries. The first library `android-database-utilities`
exposes a public API that the client code interacts with and must be available at compile time and runtime.
The second library is the `android-database-utilities-compiler`, and it contains the Java Annotation Processor
implementation for this DBAL and must be available at compile time. These two libraries are distributed as JARs
but for use on android the APT gradle plugin should also be applied to the project to ensure that the generated
files are saved to an appropriate location for compilation by the Android SDK.

## How to use

A plain java class needs to be created that represent sthe fields of a specific entity.
This class needs to be annotated with the `SQLiteEntity` class. The annotation processor will then
find these classes and analyze them to identify the fields that define the attributes of the associated entity.
The processor will automatically recognize the available constructors and align the fields according to sensible
heuristics and generate the appropriate Java code to instantiate the entities from Android cursors without ANY
reflection EVER, for a ZERO runtime impact on performance. The code generated is also sensible enough for you
to debug manually if there is a problem.

The `SQLiteDatabase` class can also be used to define databases which can be associated with entities and the
processor will automatically generate appropriate Android OpenHelpers that will take care of initializing the
database structure based on the associated entities.

There are annotations available to customize what the generator will understand as it is analyzing the class: see
`io.tensai.android.databaseutilities.interfaces.annotations.repository` for all the available annotations.

## Sample code

    @SQLiteDatabase(name = TestDatabase.NAME, version = TestDatabase.VERSION)
    public class TestDatabase {
        public static final String NAME = "testDatabase.db";
        public static final int VERSION = 1;

    }

    @SQLiteEntity(database = TestDatabase.NAME)
    public class DummyData implements Comparable<DummyData> {
        private long mPk;
        private String mFirstName;
        private String mLastName;
        private int mAge;
        private Date mDate;


        public DummyData(final long pk, final String firstName, final Date date, final String lastName, final int age) {
            mPk = pk;
            mFirstName = firstName;
            mDate = date;
            mLastName = lastName;
            mAge = age;
        }

        public long getPk() {
            return mPk;
        }

        public void setPk(final long pk) {
            mPk = pk;
        }

        public String getFirstName() {
            return mFirstName;
        }

        public void setFirstName(final String firstName) {
            mFirstName = firstName;
        }

        public String getLastName() {
            return mLastName;
        }

        public void setLastName(final String lastName) {
            mLastName = lastName;
        }

        public int getAge() {
            return mAge;
        }

        public void setAge(final int age) {
            mAge = age;
        }

        public Date getDate() {
            return mDate;
        }

        public void setDate(final Date date) {
            mDate = date;
        }
    }

    final List<DummyData> records = EntityManager.getEntityRepository(mContext, DummyData.class).findAll();


## Notes

This library depends on the `java-annotation-processor-utilities` library, but a 'bundled' JAR is also available
which includes this library and the `android-database-utilities-compiler` as one JAR artifact.

## License

LGPL-v3
